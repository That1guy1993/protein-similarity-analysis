/*$(function() {
	$("#Tables").tabs();
});*/



$(document).ready(function()
{

$('#Run').click(function(AA)

{	
var ProteinLength = 0;
var SequenceofInterest = document.getElementById('Sequence Of Interest').value;
var ProteinsArrayText = document.getElementById('Protein Sequences').value;
//-- REGEX out FASTA titles
var SequenceofInterest = SequenceofInterest.replace(/.*>.*\n/g, '');
var ProteinsArrayText = ProteinsArrayText.replace(/.*>.*/g, '');


var ProteinsArray = ProteinsArrayText.split("\n\n");

var LongestProtein = 0;
for (var i = 0; i<ProteinsArray.length; i++)
{
	if (ProteinsArray[i].length > ProteinLength)
	{
		var ProteinLength = ProteinsArray[i].length;
		LongestProtein = ProteinsArray[i].length;
	}
}


//Raw Numbers table
var AlanineRaw = [""];
var ArginineRaw = [""];
var AsparagineRaw = [""];
var AsparticAcidRaw = [""];
var CysteineRaw = [""];
var GlutamicAcidRaw = [""];
var GlutamineRaw = [""];
var GlycineRaw = [""];
var HistidineRaw = [""];
var IsoleucineRaw = [""];
var LeucineRaw = [""];
var LysineRaw = [""];
var MethionineRaw = [""];
var PhenylalanineRaw = [""];
var ProlineRaw = [""];
var SerineRaw = [""];
var ThreonineRaw = [""];
var TryptophanRaw = [""];
var TyrosineRaw = [""];
var ValineRaw = [""];
var BlankRaw = [""];

//Percentage table
var AlanineArray = [""];
var ArginineArray = [""];
var AsparagineArray = [""];
var AsparticAcidArray = [""];
var CysteineArray = [""];
var GlutamicAcidArray = [""];
var GlutamineArray = [""];
var GlycineArray = [""];
var HistidineArray = [""];
var IsoleucineArray = [""];
var LeucineArray = [""];
var LysineArray = [""];
var MethionineArray = [""];
var PhenylalanineArray = [""];
var ProlineArray = [""];
var SerineArray = [""];
var ThreonineArray = [""];
var TryptophanArray = [""];
var TyrosineArray = [""];
var ValineArray = [""];
var BlankArray = [""];

//Properties table
var HydrophobicArray = [""]; //Glycine (G), Alanine (A), Valine (V), Leucine (L), Isoleucine (I), Methionine (M)
var PositiveCharge = [""]; //Lysine (K), Arginine (R), Histidine (H)
var NegativeCharge = [""]; //Glutamic Acid (E), Aspartic Acid (D)
var PolarPhosphorylable = [""]; //Serine (S), Threonine (T), Tyrosine (Y)
var PolarUncharged = [""]; // Proline (P), Asparagine (N), Glutamine (Q) and Cysteine (C)
var Aromatic = [""]; //Phenylalanine (F) and Tryptophan (W)
var PropertiesBlank = [""];


for (var i=0; i<LongestProtein; i++)
{
	//amino acid variables (Raw number in each position)
	var alapos=0; //Alanine
	var argpos=0; //Arginine
	var asnpos=0; //Asparagine
	var asppos=0; //Aspartic Acid
	var cyspos=0; //Cysteine
	var glupos=0; //Glutamic Acid
	var glnpos=0; //Glutamine
	var glypos=0; //Glycine
	var hispos=0; //Histidine
	var ilepos=0; //Isoleucine
	var leupos=0; //Leucine
	var lyspos=0; //Lysine
	var metpos=0; //Methionine
	var phepos=0; //Phenylalanine
	var propos=0; //Proline
	var serpos=0; //Serine
	var thrpos=0; //Threonine
	var trppos=0; //Tryptophan
	var tyrpos=0; //Tyrosine
	var valpos=0; //Valine
	var blank=0; 
	
		for(var j=0; j<ProteinsArray.length; j++)
		{
		
		if(ProteinsArray[j][i]==="A")
		{
			alapos=alapos+1;
		}
		else if (ProteinsArray[j][i]==="R")
		{
			argpos=argpos+1;
		}
		else if (ProteinsArray[j][i]==="N")
		{
			asnpos=asnpos+1;
		}
		else if (ProteinsArray[j][i]==="D")
		{
			asppos=asppos+1;
		}
		else if (ProteinsArray[j][i]==="C")
		{
			cyspos=cyspos+1;
		}
		else if (ProteinsArray[j][i]==="E")
		{
			glupos=glupos+1;
		}
		else if (ProteinsArray[j][i]==="Q")
		{
			glnpos=glnpos+1;
		}
		else if (ProteinsArray[j][i]==="G")
		{
			glypos=glypos+1;
		}
		else if (ProteinsArray[j][i]==="H")
		{
			hispos=hispos+1;
		}
		else if (ProteinsArray[j][i]==="I")
		{
			ilepos=ilepos+1;
		}
		else if (ProteinsArray[j][i]==="L")
		{
			leupos=leupos+1;
		}
		else if (ProteinsArray[j][i]==="K")
		{
			lyspos=lyspos+1;
		}
		else if (ProteinsArray[j][i]==="M")
		{
			metpos=metpos+1;
		}
		else if (ProteinsArray[j][i]==="F")
		{
			phepos=phepos+1;
		}
		else if (ProteinsArray[j][i]==="P")
		{
			propos=propos+1;
		}
		else if (ProteinsArray[j][i]==="S")
		{
			serpos=serpos+1;
		}
		else if (ProteinsArray[j][i]==="T")
		{
			thrpos=thrpos+1;
		}
		else if (ProteinsArray[j][i]==="W")
		{
			trppos=trppos+1;
		}
		else if (ProteinsArray[j][i]==="Y")
		{
			tyrpos=tyrpos+1;
		}
		else if (ProteinsArray[j][i]==="V")
		{
			valpos=valpos+1;
		}
		else 
		{
			blank++;
		}	
		}
	
	console.log(i);
	var Alanine=(Math.round(alapos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	AlanineArray.push(Alanine);
	AlanineRaw.push(alapos);
	var Arginine=(Math.round(argpos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	ArginineArray.push(Arginine);
	ArginineRaw.push(argpos);
	var Asparagine=(Math.round(asnpos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	AsparagineArray.push(Asparagine);
	AsparagineRaw.push(asnpos);
	var AsparticAcid=(Math.round(asppos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	AsparticAcidArray.push(AsparticAcid);
	AsparticAcidRaw.push(asppos);
	var Cysteine=(Math.round(cyspos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	CysteineArray.push(Cysteine);
	CysteineRaw.push(cyspos);
	var GlutamicAcid=(Math.round(glupos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	GlutamicAcidArray.push(GlutamicAcid);
	GlutamicAcidRaw.push(glupos);
	var Glutamine=(Math.round(glnpos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	GlutamineArray.push(Glutamine);
	GlutamineRaw.push(glnpos);
	var Glycine=(Math.round(glypos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	GlycineArray.push(Glycine);
	GlycineRaw.push(glypos);
	var Histidine=(Math.round(hispos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	HistidineArray.push(Histidine);
	HistidineRaw.push(hispos);
	var Isoleucine=(Math.round(ilepos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	IsoleucineArray.push(Isoleucine);
	IsoleucineRaw.push(ilepos);
	var Leucine=(Math.round(leupos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	LeucineArray.push(Leucine);
	LeucineRaw.push(leupos);
	var Lysine=(Math.round(lyspos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	LysineArray.push(Lysine);
	LysineRaw.push(lyspos);
	var Methionine=(Math.round(metpos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	MethionineArray.push(Methionine);
	MethionineRaw.push(metpos);
	var Phenylalanine=(Math.round(phepos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	PhenylalanineArray.push(Phenylalanine);
	PhenylalanineRaw.push(phepos);
	var Proline=(Math.round(propos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	ProlineArray.push(Proline);
	ProlineRaw.push(propos);
	var Serine=(Math.round(serpos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	SerineArray.push(Serine);
	SerineRaw.push(serpos);
	var Threonine=(Math.round(thrpos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	ThreonineArray.push(Threonine);
	ThreonineRaw.push(thrpos);
	var Tryptophan=(Math.round(trppos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	TryptophanArray.push(Tryptophan);
	TryptophanRaw.push(trppos);
	var Tyrosine=(Math.round(tyrpos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	TyrosineArray.push(Tyrosine);
	TyrosineRaw.push(tyrpos);
	var Valine=(Math.round(valpos/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	ValineArray.push(Valine);
	ValineRaw.push(valpos);
	var Blank=(Math.round(blank/(alapos+argpos+asnpos+asppos+cyspos+glupos+glnpos+glypos+hispos+ilepos+leupos+lyspos+metpos+phepos+propos+serpos+thrpos+trppos+tyrpos+valpos+blank)*100*10)/10);
	BlankArray.push(Blank);
	BlankRaw.push(blank);
	
	
	//Properties Adding
	NegativeCharge.push(Math.round(((asppos + glupos)/ProteinsArray.length)*100*10)/10);
	PositiveCharge.push(Math.round(((lyspos + argpos + hispos)/ProteinsArray.length)*100*10)/10);
	Aromatic.push(Math.round(((phepos + trppos)/ProteinsArray.length)*100*10)/10);
	PolarPhosphorylable.push(Math.round(((serpos + thrpos + tyrpos)/ProteinsArray.length)*100*10)/10);
	PolarUncharged.push(Math.round(((propos + asnpos + glnpos + cyspos)/ProteinsArray.length)*100 *10)/10);
	HydrophobicArray.push(Math.round(((glypos + alapos + valpos + leupos + ilepos + metpos)/ProteinsArray.length)*100 *10)/10);
	PropertiesBlank.push(Math.round(((blank)/ProteinsArray.length)*100*10)/10);		
}


//RAW data

var TableContents ="";
var TableHeader ="";
var TableFooter ="";

TableHeader = ("<h1>Raw data- number of times each amino acid appears at a position</h1><div style='overflow-x:auto;'><table border='1' id='RAW'><tr><td>Sequence of Interest</td>");

for (var y=0; y<SequenceofInterest.length; y++)
{
	TableContents+=("<td>" + SequenceofInterest[y] + "</td>");
}
TableContents+=("</tr><tr><td>Position</td>");

for (var x=0; x<LongestProtein; x++)
{
	TableContents+=("<td>" + x + "</td>");
}

//Alanine 
TableContents+=("</tr><tr><td>Alanine (A)</td>")
for (var q=1; q < AlanineRaw.length; q++)
{
	TableContents+=("<td>" + AlanineRaw[q] + "</td>");
}

//Arginine 
TableContents+=("</tr><tr><td>Arginine (R)</td>")
for (var q=1; q < ArginineRaw.length; q++)
{
	TableContents+=("<td>" + ArginineRaw[q] + "</td>");
}

//Asparagine 
TableContents+=("</tr><tr><td>Asparagine (N)</td>")
for (var q=1; q < AsparagineRaw.length; q++)
{
	TableContents+=("<td>" + AsparagineRaw[q] + "</td>");
}

//Aspartic Acid 
TableContents+=("</tr><tr><td>Aspartic Acid (D)</td>")
for (var q=1; q < AsparagineRaw.length; q++)
{
	TableContents+=("<td>" + AsparticAcidRaw[q] + "</td>");
}

//Cysteine 
TableContents+=("</tr><tr><td>Cysteine (C)</td>")
for (var q=1; q < CysteineRaw.length; q++)
{
	TableContents+=("<td>" + CysteineRaw[q] + "</td>");
}

//Glutamic Acid 
TableContents+=("</tr><tr><td>Glutamic Acid (E)</td>")
for (var q=1; q < GlutamicAcidRaw.length; q++)
{
	TableContents+=("<td>" + GlutamicAcidRaw[q] + "</td>");
}

//Glutamine
TableContents+=("</tr><tr><td>Glutamine (Q)</td>")
for (var q=1; q < GlutamineRaw.length; q++)
{
	TableContents+=("<td>" + GlutamineRaw[q] + "</td>");
}

//Glycine
TableContents+=("</tr><tr><td>Glycine (G)</td>")
for (var q=1; q < GlycineRaw.length; q++)
{
	TableContents+=("<td>" + GlycineRaw[q] + "</td>");
}

//Histidine
TableContents+=("</tr><tr><td>Histidine (H)</td>")
for (var q=1; q < HistidineRaw.length; q++)
{
	TableContents+=("<td>" + HistidineRaw[q] + "</td>");
}

//Isoleucine
TableContents+=("</tr><tr><td>Isoleucine (I)</td>")
for (var q=1; q < IsoleucineRaw.length; q++)
{
	TableContents+=("<td>" + IsoleucineRaw[q] + "</td>");
}

//Leucine
TableContents+=("</tr><tr><td>Leucine (L)</td>")
for (var q=1; q < LeucineRaw.length; q++)
{
	TableContents+=("<td>" + LeucineRaw[q] + "</td>");
}

//Lysine
TableContents+=("</tr><tr><td>Lysine (K)</td>")
for (var q=1; q < LysineRaw.length; q++)
{
	TableContents+=("<td>" + LysineRaw[q] + "</td>");
}

//Methionine
TableContents+=("</tr><tr><td>Methionine (M)</td>")
for (var q=1; q < MethionineRaw.length; q++)
{
	TableContents+=("<td>" + MethionineRaw[q] + "</td>");
}

//Phenylalanine
TableContents+=("</tr><tr><td>Phenylalanine (F)</td>")
for (var q=1; q < PhenylalanineRaw.length; q++)
{
	TableContents+=("<td>" + PhenylalanineRaw[q] + "</td>");
}

//Proline
TableContents+=("</tr><tr><td>Proline (P)</td>")
for (var q=1; q < ProlineRaw.length; q++)
{
	TableContents+=("<td>" + ProlineRaw[q] + "</td>");
}

//Serine
TableContents+=("</tr><tr><td>Serine (S)</td>")
for (var q=1; q < SerineRaw.length; q++)
{
	TableContents+=("<td>" + SerineRaw[q] + "</td>");
}

//Threonine
TableContents+=("</tr><tr><td>Threonine (T)</td>")
for (var q=1; q < ThreonineRaw.length; q++)
{
	TableContents+=("<td>" + ThreonineRaw[q] + "</td>");
}

//Tryptophan
TableContents+=("</tr><tr><td>Tryptophan (W)</td>")
for (var q=1; q < TryptophanRaw.length; q++)
{
	TableContents+=("<td>" + TryptophanRaw[q] + "</td>");
}

//Tyrosine
TableContents+=("</tr><tr><td>Tyrosine (Y)</td>")
for (var q=1; q < TyrosineRaw.length; q++)
{
	TableContents+=("<td>" + TyrosineRaw[q] + "</td>");
}

//Valine
TableContents+=("</tr><tr><td>Valine (V)</td>")
for (var q=1; q < ValineRaw.length; q++)
{
	TableContents+=("<td>" + ValineRaw[q] + "</td>");
}

//Blank
TableContents+=("</tr><tr><td>Blank (-)</td>")
for (var q=1; q < BlankRaw.length; q++)
{
	TableContents+=("<td>" + BlankRaw[q] + "</td>");
}

TableFooter = ("</tr></table></div>");
var Table = TableHeader + TableContents + TableFooter;
$('#RAW').html(Table);


//Amino acid percentages
var TableContentsPercentages ="";
var TableHeader1Percentages ="";
var TableFooter1Percentages ="";

TableHeader1Percentages = ("<h1>Percentage of times each amino acid appears at a position</h1><div style='overflow-x:auto;'><table border='1' id='Percentages'><tr><td>Sequence of Interest</td>");

for (var y=0; y<SequenceofInterest.length; y++)
{
	TableContentsPercentages+=("<td>" + SequenceofInterest[y] + "</td>");
}
TableContentsPercentages+=("</tr><tr><td>Position</td>");

for (var x=0; x<LongestProtein; x++)
{
	TableContentsPercentages+=("<td>" + x + "</td>");
}

//Alanine
TableContentsPercentages+=("</tr><tr><td>Alanine (A)</td>")
for (var q=1; q < AlanineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + AlanineArray[q] + "</td>");
}

//Arginine
TableContentsPercentages+=("</tr><tr><td>Arginine (R)</td>")
for (var q=1; q < ArginineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + ArginineArray[q] + "</td>");
}

//Asparagine
TableContentsPercentages+=("</tr><tr><td>Asparagine (N)</td>")
for (var q=1; q < AsparagineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + AsparagineArray[q] + "</td>");
}

//Aspartic Acid
TableContentsPercentages+=("</tr><tr><td>Aspartic Acid (D)</td>")
for (var q=1; q < AsparagineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + AsparticAcidArray[q] + "</td>");
}

//Cysteine
TableContentsPercentages+=("</tr><tr><td>Cysteine (C)</td>")
for (var q=1; q < CysteineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + CysteineArray[q] + "</td>");
}

//GlutamicAcid
TableContentsPercentages+=("</tr><tr><td>Glutamic Acid (E)</td>")
for (var q=1; q < GlutamicAcidArray.length; q++)
{
	TableContentsPercentages+=("<td>" + GlutamicAcidArray[q] + "</td>");
}

//Glutamine
TableContentsPercentages+=("</tr><tr><td>Glutamine (Q)</td>")
for (var q=1; q < GlutamineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + GlutamineArray[q] + "</td>");
}

//Glycine
TableContentsPercentages+=("</tr><tr><td>Glycine (G)</td>")
for (var q=1; q < GlycineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + GlycineArray[q] + "</td>");
}

//Histidine
TableContentsPercentages+=("</tr><tr><td>Histidine (H)</td>")
for (var q=1; q < HistidineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + HistidineArray[q] + "</td>");
}

//Isoleucine
TableContentsPercentages+=("</tr><tr><td>Isoleucine (I)</td>")
for (var q=1; q < IsoleucineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + IsoleucineArray[q] + "</td>");
}

//Leucine
TableContentsPercentages+=("</tr><tr><td>Leucine (L)</td>")
for (var q=1; q < LeucineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + LeucineArray[q] + "</td>");
}

//Lysine
TableContentsPercentages+=("</tr><tr><td>Lysine (K)</td>")
for (var q=1; q < LysineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + LysineArray[q] + "</td>");
}

//Methionine
TableContentsPercentages+=("</tr><tr><td>Methionine (M)</td>")
for (var q=1; q < MethionineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + MethionineArray[q] + "</td>");
}

//Phenylalanine
TableContentsPercentages+=("</tr><tr><td>Phenylalanine (F)</td>")
for (var q=1; q < PhenylalanineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + PhenylalanineArray[q] + "</td>");
}

//Proline
TableContentsPercentages+=("</tr><tr><td>Proline (P)</td>")
for (var q=1; q < ProlineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + ProlineArray[q] + "</td>");
}

//Serine
TableContentsPercentages+=("</tr><tr><td>Serine (S)</td>")
for (var q=1; q < SerineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + SerineArray[q] + "</td>");
}

//Threonine
TableContentsPercentages+=("</tr><tr><td>Threonine (T)</td>")
for (var q=1; q < ThreonineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + ThreonineArray[q] + "</td>");
}

//Tryptophan
TableContentsPercentages+=("</tr><tr><td>Tryptophan (W)</td>")
for (var q=1; q < TryptophanArray.length; q++)
{
	TableContentsPercentages+=("<td>" + TryptophanArray[q] + "</td>");
}

//Tyrosine
TableContentsPercentages+=("</tr><tr><td>Tyrosine (Y)</td>")
for (var q=1; q < TyrosineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + TyrosineArray[q] + "</td>");
}

//Valine
TableContentsPercentages+=("</tr><tr><td>Valine (V)</td>")
for (var q=1; q < ValineArray.length; q++)
{
	TableContentsPercentages+=("<td>" + ValineArray[q] + "</td>");
}

//Blank
TableContentsPercentages+=("</tr><tr><td>Blank (-)</td>")
for (var q=1; q < BlankArray.length; q++)
{
	TableContentsPercentages+=("<td>" + BlankArray[q] + "</td>");
}

TableFooter1Percentages = ("</tr></table></div>");
var PercentagesTable = TableHeader1Percentages + TableContentsPercentages + TableFooter1Percentages;
$('#Percentages').html(PercentagesTable);


//Properties Table
var TableContentsProperties ="";
var TableHeader1Properties ="";
var TableFooter1Properties ="";

TableHeader1Properties = ("<br><br><h1>Properties - percentage of times a specific amino acid property appears at a position</h1><div style='overflow-x:auto;'><table border='1'  style='border-collapse: collapse' id='Properties'><tr><td>Sequence of Interest</td>");

for (var y=0; y<SequenceofInterest.length; y++)
{
	TableContentsProperties+=("<td>" + SequenceofInterest[y] + "</td>");
}
TableContentsProperties+=("</tr><tr><td>Position</td>");

for (var x=0; x<LongestProtein; x++)
{
	TableContentsProperties+=("<td>" + x + "</td>");
}

//Hydrophobic
TableContentsProperties+=("</tr><tr><td>Hydrophobic (A, G, I, L, M, V)</td>")
for ( var q=1; q < HydrophobicArray.length; q++)
{
	TableContentsProperties+=("<td>" + HydrophobicArray[q] + "</td>");
}

//Positive Charge
TableContentsProperties+=("</tr><tr><td>Positive Charge (H, K, R)</td>")
for ( var q=1; q < PositiveCharge.length; q++)
{
	TableContentsProperties+=("<td>" + PositiveCharge[q] + "</td>");
}

//Negative Charge
TableContentsProperties+=("</tr><tr><td>Negative Charge (D, E)</td>")
for ( var q=1; q < NegativeCharge.length; q++)
{
	TableContentsProperties+=("<td>" + NegativeCharge[q] + "</td>");
}

//Polar Phosphorylable
TableContentsProperties+=("</tr><tr><td>Polar Phosphorylable (S, T, Y)</td>")
for ( var q=1; q < PolarPhosphorylable.length; q++)
{
	TableContentsProperties+=("<td>" + PolarPhosphorylable[q] + "</td>");
}

//Polar Uncharged
TableContentsProperties+=("</tr><tr><td>Polar Uncharged (C, N, P, Q)</td>")
for ( var q=1; q < PolarUncharged.length; q++)
{
	TableContentsProperties+=("<td>" + PolarUncharged[q] + "</td>");
}

//Blank
TableContentsProperties+=("</tr><tr><td>Blank (-)</td>")
for ( var q=1; q < PropertiesBlank.length; q++)
{
	TableContentsProperties+=("<td>" + PropertiesBlank[q] + "</td>");
}

TableFooter1Properties=("</tr></table></div>")

var PropertiesTable = TableHeader1Properties + TableContentsProperties + TableFooter1Properties;
$('#Properties').html(PropertiesTable);
});
});