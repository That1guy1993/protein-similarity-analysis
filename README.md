### Request ###

* This code has been written as part of a PhD and MSc projects, if used please contact Leonard Daly (hlldaly [at] student [dot] liverpool [dot] ac [dot] uk) in order to evaluate if any impact was made in writing this script.


# Protein similarity analysis #

This javascript script is written to automate the extraction of numerical data from protein alignments, including the number of times a specific amino acid appears at every position (Raw data), the percentage of times a specific amino acid appears at every position (Percentages data) and the percentage of times a particular amino acid property appears at every position e.g. hydrophobic, polar etc (Properties data).

### How do I get set up? ###

* Protein alignments need to be performed beforehand. Your program of choice should work IF the alignments can be extracted in FASTA format (tested with COBALT and MUSCLE multiple sequence alignment programs by NCBI and EMBL-EBI respectively).
* Open the Protein Alignment.html file
* If there is a sequence of particular interest (e.g. Human, Mouse etc) copy this aligned FASTA sequence into the Sequence of Interest box, this will appear as a table header for easy comparison to numerical data
* Copy all aligned sequences in FASTA format into the Aligned Protein Sequences box (include the sequence of interest here too)
* Click run, all data appears as tables in the HTML page. Each table can be exported as .xls files to Excel to save and analyse further.

### Acknowledgements and Licence ###

* This code was written by Leonard Daly, from University of Liverpool, and Samuel Scrutton, from Manchester Metropolitan University.
* Provided under an MIT Licence (see licence file).
